var https = require('https');
var express = require('express');
var httpProxy = require('http-proxy');
var log4js = require('log4js');

log4js.configure({
    appenders: [
        { type: 'console' },
        { type: 'file', filename: 'https.log', category: 'httptohttps' }
    ]
});


var logger = log4js.getLogger();
var whitelist = require("./whitelist.json").url;
var url = require('url');

var app = express();


// Change to port 80, 3000 is used for development purposes

var PORT = 3000 || env.PORT;
var USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';

var proxy = httpProxy.createProxyServer();

proxy.on('proxyReq', function (proxyReq, req, res) {
    proxyReq.setHeader('user-agent',USER_AGENT);
    proxyReq.path = proxyReq.path.slice(0, -5);
    if(proxyReq.path.length > 1){
        proxyReq.path = proxyReq.path.slice(0, -1)
    }

})
proxy.on('error',function(err, req, res){
    logger.error('Error occured '+err);
    res.sendStatus(500);
})

app.all('/proxy',function(req, res, next){
    var destination = req.header('destination');

    if (destination == undefined){
        return res.sendStatus(400)
    }

    var formatedUrl = url.parse(destination, true, true);
    var hostname = formatedUrl.hostname;
    for(var i = 0; i <= whitelist.length; i++){
        if(hostname.indexOf(whitelist[i]) > -1){
            proxy.web(req, res ,{
                target: destination,
                agent: https.globalAgent,
                headers:{
                    host: hostname
                }
            })
            return next;
        }
    }
    return next(res.sendStatus(403))
})

app.listen(PORT, function(){
    logger.info('Server running on port: '+PORT)
})